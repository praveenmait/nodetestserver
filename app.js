var request = require('request');
var express = require('express');
var app  = express();
var myParser = require('body-parser');

var port = "4545";
var sessionURL = "";
var url = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0/";
var apiKey = "lo963355623698263789439888313183";
var params = {
    "apiKey":apiKey,
    "country":"GB",
    "locale":"en-US",
    "originplace":"DEL",
    "destinationplace":"BLR",
    "inbounddate":"2018-11-14",
    "outbounddate":"2018-11-07",
    "adults":1,
    "children":0,
    "locationschema":"iata",
    "cabinclass":"Economy",
    "preferDirects":false,
    "grouppricing":false,
    "Currency":"GBP"
  };



function getSessionURL(callback){
  request.post({
  	url:url,
  	form:params
  	}, function(err,response,body){
  		if(err) {
  			console.log(err);
        return false;
  		}else{
        callback(response.headers.location);
  		}
  	}
  );
}


function getLiveData(sessionurl,callback){
  request.get(sessionurl,
    function(err,response,body){
      if(err) {
        callback(err,false);
      }else{
        callback(body,true);
      }
    }
  )
}

getSessionURL(function(url){
  sessionURL = url + "?apiKey=" + apiKey;
  app.listen(port);
  console.log("listening on port : " + port);
  console.log("got session url: " + url);
});

app.use(myParser.json());

app.get("/api/test",function(req,res){
  getLiveData(sessionURL,function(response,bool){
    if(bool){
      res.send(response);
    }else{
      res.send("Error lol");
    }
  });
});